<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tasks', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps('start_time');
            $table->string('name');
            $table->text('description');
            $table->integer('estimateTime');

            $table->integer("sequence");

            $table->integer('project_id')->unsigned();
            $table->foreign('project_id')->references('id')->on('project');
		});


		Schema::create('task_user', function(Blueprint $table)
		{
			$table->integer('user_id')->unsigned()->index();
			$table->foreign('user_id')->references('id')->on('users');

			$table->integer('task_id')->unsigned()->index();
			$table->foreign('task_id')->references('id')->on('tasks');

			$table->timestamps();

		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('tasks');
		Schema::drop('user_task');
	}

}
