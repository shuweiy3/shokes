[33mcommit 83bc27e6fa7d9ef8ac1a7f4c9ee1f07f4bace6fa[m
Merge: 084bffc 4839cbe
Author: SHUWEI YANG <shuweiy3@gmail.com>
Date:   Fri Jun 12 20:21:50 2015 -0700

    Merge branch 'master' of https://bitbucket.org/endqwerty/adamrepublics
    
    Conflicts:
    	public/css/Xlab.css
    	public/css/lab.css
    	public/css/xlab.css
    	resources/views/project/index.blade.php
    	resources/views/project/profile.blade.php

[33mcommit 084bffcbd33df974c8838947524e809475af14a3[m
Author: SHUWEI YANG <shuweiy3@gmail.com>
Date:   Fri Jun 12 17:52:46 2015 -0700

    Update

[33mcommit 77980988678ce712606372fd2c636d79d302e2f8[m
Author: SHUWEI YANG <shuweiy3@gmail.com>
Date:   Mon Jun 8 20:57:36 2015 -0700

    The update

[33mcommit 4839cbe4af00732f82e9c4da9a643311db7a7144[m
Author: Daniel <endqwerty@gmail.com>
Date:   Sun May 17 10:17:16 2015 +0000

    dynamic profiles

[33mcommit 1e081511626d6ab132fb2a0532679fbc959cdabe[m
Author: Daniel <endqwerty@gmail.com>
Date:   Sun May 17 10:09:26 2015 +0000

    db change to over 255 word count

[33mcommit adc0900efdc4f8cb7ee77eddbeaea5dbeb54aa18[m
Author: Shuwei Yang <shuweiy@uci.edu>
Date:   Sun May 17 00:34:53 2015 -0700

    Update the seed database need to update the seed database into the project page

[33mcommit 69460a85bc5cdafac2b4d2022bd44f79e2bae6ac[m
Author: Shuwei Yang <shuweiy@uci.edu>
Date:   Thu May 14 19:41:43 2015 -0700

    Adding flip effect

[33mcommit caae741d518b0104cd0ca2570ec7b5a124316149[m
Merge: 1cc21e5 9355a69
Author: Shuwei Yang <shuweiy@uci.edu>
Date:   Thu May 14 18:19:26 2015 -0700

    Merge branch 'master' of https://bitbucket.org/endqwerty/adamrepublics

[33mcommit 1cc21e5b9f2354e81b4adb743bd08dc425a5c1f9[m
Author: Shuwei Yang <shuweiy@uci.edu>
Date:   Thu May 14 17:39:36 2015 -0700

    Update project page

[33mcommit 9355a6935e1f30312c6293e19296dc08c5f722c5[m
Author: Daniel <endqwerty@gmail.com>
Date:   Thu May 14 02:08:51 2015 +0000

    reduce image size

[33mcommit d4972cef86dfb7a19c44d91502850c22e6c206ee[m
Author: Daniel <endqwerty@gmail.com>
Date:   Thu May 14 00:17:42 2015 +0000

    add projects list link

[33mcommit 3ab2b756daa97f48d593fb29b2013a7896cfa539[m
Merge: ae8df61 a157eb8
Author: Daniel <endqwerty@gmail.com>
Date:   Thu May 14 00:15:51 2015 +0000

    Merge branch 'homepage_redirects' of https://bitbucket.org/endqwerty/adamrepublics into homepage_redirects

[33mcommit a157eb8a9fbabfbc3b0625a6cd3a63ecd11072bf[m
Author: Daniel <endqwerty@gmail.com>
Date:   Thu May 14 00:13:51 2015 +0000

    rename

[33mcommit ae8df61be4fdbe2d6a866411d0ee289b2cd645c0[m
Author: Daniel <endqwerty@gmail.com>
Date:   Thu May 14 00:05:54 2015 +0000

    add blade templating

[33mcommit c732bd7e812a4be714ac5fe60c2dc667ed2594bf[m
Author: Daniel <endqwerty@gmail.com>
Date:   Thu May 14 00:02:50 2015 +0000

    fixcase

[33mcommit 836e98ed756e4ec0467a2acd3156d36e9fd69c70[m
Author: Daniel <endqwerty@gmail.com>
Date:   Wed May 13 11:33:35 2015 +0000

    refactor welcome page css

[33mcommit 8facfa3c9e09ff6d2ab3f04b29ca7c1ca2edee50[m
Author: Daniel <endqwerty@gmail.com>
Date:   Wed May 13 11:09:22 2015 +0000

    refactor test to welcome and add auth check to homepage

[33mcommit f8629f5b00a59c1a315c62c448a216628a5f909d[m
Merge: 3c7d09d 9988e24
Author: Daniel <endqwerty@gmail.com>
Date:   Wed May 13 10:49:56 2015 +0000

    Merge branch 'fix_project'
    
    Conflicts:
    	resources/views/project/profile.blade.php
    	resources/views/project/timeline.blade.php

[33mcommit 9988e248a2d78a4d62a65b58f0945b5ca919a8f1[m
Author: Daniel <endqwerty@gmail.com>
Date:   Wed May 13 10:47:10 2015 +0000

    add projectlist css, switch default font to Roboto

[33mcommit 85535d2148935569b9641657a5c5659d9315d454[m
Author: Daniel <endqwerty@gmail.com>
Date:   Wed May 13 09:25:13 2015 +0000

    refactoring

[33mcommit 3e4cb282a6a1f2cb5930e870e7aa27221fddb7ad[m
Author: Daniel <endqwerty@gmail.com>
Date:   Wed May 13 09:09:02 2015 +0000

    change to tables and refactoring timeline.js

[33mcommit f168f8841298e454bc5086cfd61865d513ca8762[m
Author: Daniel <endqwerty@gmail.com>
Date:   Wed May 13 09:00:23 2015 +0000

    create project list page

[33mcommit 3c7d09d13e9708dafd8e71449666cbdfe32017c7[m
Author: Shuwei Yang <shuweiy@uci.edu>
Date:   Wed May 13 01:49:05 2015 -0700

    The process project profile

[33mcommit 4b19935395e58f63b05f5e99ebb6171e9451e1b2[m
Author: Daniel <endqwerty@gmail.com>
Date:   Wed May 13 08:42:14 2015 +0000

    project page using blocks

[33mcommit e3c37e41ad55ace4c73ba160aa1c702594cc016f[m
Author: Daniel <endqwerty@gmail.com>
Date:   Tue May 12 06:26:39 2015 +0000

    refactoring

[33mcommit a4ed3f459724d35bb2a84623c54fc91e2a8f0a06[m
Author: Daniel <endqwerty@gmail.com>
Date:   Tue May 12 04:07:42 2015 +0000

    fix header spacing

[33mcommit 101c74ee58f68f683f36d74655dffe6a644e1b86[m
Author: Daniel <endqwerty@gmail.com>
Date:   Tue May 12 03:59:01 2015 +0000

    composer update

[33mcommit 97af2c5fe91f83ab0194d9edfdc2a9afd5d71264[m
Merge: 8fdbc99 9ffc347
Author: Daniel <endqwerty@gmail.com>
Date:   Tue May 12 03:53:43 2015 +0000

    Merge branch 'Homepage'
    
    Conflicts:
    	.gitignore
    	app/Http/Controllers/TaskController.php
    	public/css/project.css

[33mcommit 9ffc347ab5a08701d3698022746281876432ce91[m
Author: Shuwei Yang <shuweiy@uci.edu>
Date:   Mon May 11 14:36:38 2015 -0700

    Check test page

[33mcommit af3cd5109994eb9a4c24b0f8ce281923d0f33006[m
Author: Daniel <endqwerty@gmail.com>
Date:   Wed May 6 22:33:41 2015 +0000

    moving css over

[33mcommit e595caa289cfcd5c20dd79f094bfca87fb62b543[m
Author: Daniel <endqwerty@gmail.com>
Date:   Wed May 6 20:39:45 2015 +0000

    temp fixes

[33mcommit feed26e4e382add2e2ada341f00da636b527b3c9[m
Author: Daniel <endqwerty@gmail.com>
Date:   Wed May 6 20:20:01 2015 +0000

    refactor animation.js

[33mcommit f5067e2af9ab4a02fbe3346e13d5b27f4f060ba7[m
Author: Daniel <endqwerty@gmail.com>
Date:   Wed May 6 20:02:38 2015 +0000

    fix gitignore

[33mcommit ba1dd559e56dffb08b4f2b06e06c1d90703d0c0d[m
Author: Daniel <endqwerty@gmail.com>
Date:   Wed May 6 19:57:09 2015 +0000

    fix css error

[33mcommit 2091ede51af3b3aabd6efb7960a73020744b5535[m
Author: Shuwei Yang <shuweiy@uci.edu>
Date:   Tue May 5 17:48:10 2015 -0700

    Home Page+ Project Page
    Need Help on Layout

[33mcommit fd00dd3e0573e17c3c9a89bb7d9c054d19b567de[m
Merge: 910795f bf35775
Author: Shuwei Yang <shuweiy@uci.edu>
Date:   Tue May 5 15:42:31 2015 -0700

    Merge branch 'Homepage' of https://bitbucket.org/endqwerty/adamrepublics into Homepage
    
    Conflicts:

[33mcommit 910795f79e3324a13c757d230416e04f4ce68c73[m
Author: Shuwei Yang <shuweiy@uci.edu>
Date:   Tue May 5 01:09:25 2015 -0700

    Image update

[33mcommit bf35775ad11429cb89d2b6bfd4e4a609f74b0ca9[m
Author: Daniel <endqwerty@gmail.com>
Date:   Tue May 5 02:28:21 2015 +0000

    fix project

[33mcommit 8fdbc99007ba543b8e32741236d22398f480aba8[m
Author: Daniel <endqwerty@gmail.com>
Date:   Tue May 5 02:22:32 2015 +0000

    move timeline into folder

[33mcommit 58e79ef3a6a2c61a958e7f5a99681b2d886eba1a[m
Merge: e379855 a9e8421
Author: Shuwei Yang <shuweiy@uci.edu>
Date:   Mon May 4 19:19:31 2015 -0700

    Merge branch 'Alex_Work' into Homepage
    
    Conflicts:
    	resources/views/project.blade.php

[33mcommit a9e8421afba0c7a5384e59d3da2679731ca725cc[m
Author: Shuwei Yang <shuweiy@uci.edu>
Date:   Mon May 4 19:11:43 2015 -0700

    Homepage picture

[33mcommit dd37bfc080e1e439338dc126991a7f95d5554b0d[m
Author: Daniel <endqwerty@gmail.com>
Date:   Tue May 5 02:06:23 2015 +0000

    add project back

[33mcommit 58279818fec9146faa3b63b7d861cdfd2e692a51[m
Author: Daniel <endqwerty@gmail.com>
Date:   Tue May 5 02:05:38 2015 +0000

    remove project

[33mcommit 6530ef338976fbc2b5a12afb1b8b22c9460d5e57[m
Author: Daniel <endqwerty@gmail.com>
Date:   Tue May 5 02:02:33 2015 +0000

    modified attributes

[33mcommit 419995544baa3d38703ef9e6b799827c742b3a4a[m
Author: Daniel <endqwerty@gmail.com>
Date:   Tue May 5 01:58:14 2015 +0000

    fix gitignore

[33mcommit 12071d22e330b47fb77d7750c5cb0849127575a4[m
Author: Daniel <endqwerty@gmail.com>
Date:   Tue May 5 01:56:50 2015 +0000

    modified gitignore

[33mcommit 1f4f87c7994796c72139df549b881d4640c89c16[m
Author: Shuwei Yang <shuweiy@uci.edu>
Date:   Mon May 4 17:39:30 2015 -0700

    homepage updates

[33mcommit 9c1d69cd2cd67ae6d3463cdfd6ccc84440f55878[m
Merge: 29fbfd1 b735ecc
Author: Shuwei Yang <shuweiy@uci.edu>
Date:   Mon May 4 17:37:29 2015 -0700

    Merge branch 'Alex_Work' of https://bitbucket.org/endqwerty/adamrepublics into Alex_Work

[33mcommit e3798550467d8f9f0b6d9a9fad204dc88d7915fb[m
Author: Daniel <endqwerty@gmail.com>
Date:   Mon May 4 22:09:31 2015 +0000

    passing data to view from database

[33mcommit 29fbfd1f6ec6ea446cc6422338a79a5971e28a37[m
Author: Shuwei Yang <shuweiy@uci.edu>
Date:   Mon May 4 11:30:24 2015 -0700

    Revert "The home page rough"
    
    This reverts commit dc563ccbcfde02b1ebf65a45303e0c6f50fa2900.

[33mcommit dc563ccbcfde02b1ebf65a45303e0c6f50fa2900[m
Author: Shuwei Yang <shuweiy@uci.edu>
Date:   Mon May 4 11:27:10 2015 -0700

    The home page rough

[33mcommit 505d44537a5b862f752f7df9c5c1d403a9883b87[m
Author: Daniel <endqwerty@gmail.com>
Date:   Mon May 4 11:31:25 2015 +0000

    pass data to view

[33mcommit 98e8e2ce9ca07b85672c82d1d883462ec13beb1f[m
Merge: 113ddfe b735ecc
Author: Daniel <endqwerty@gmail.com>
Date:   Mon May 4 10:48:47 2015 +0000

    Merge branch 'Alex_Work'
    
    Conflicts:
    	app/Http/routes.php
    	database/migrations/2015_03_26_124634_create_project_table.php
    	database/migrations/2015_04_20_055358_create_task_table.php
    	resources/views/project/profile.blade.php

[33mcommit b735ecc66505553b34998758bdbeab214f140c14[m
Author: Daniel <endqwerty@gmail.com>
Date:   Mon May 4 10:42:26 2015 +0000

    db migration update

[33mcommit 0a84090fa962e3cd5aaa1a7dd30dfef672479352[m
Author: Daniel <endqwerty@gmail.com>
Date:   Mon May 4 02:52:57 2015 -0700

    adjusted table seeding

[33mcommit 113ddfeb07ca3f7ae60627f3c973c3122d24ff97[m
Author: Daniel <endqwerty@gmail.com>
Date:   Thu Apr 30 19:53:53 2015 +0000

    database updates

[33mcommit 4c55d34ed1b68f09117052cdb813f7b1818327b5[m
Author: Shuwei Yang <shuweiy@uci.edu>
Date:   Wed Apr 29 00:11:22 2015 -0700

    The Home Page layout

[33mcommit 3cb6a46785629110e1430ee693025a25edc7b55a[m
Author: Daniel <endqwerty@gmail.com>
Date:   Tue Apr 28 09:41:15 2015 -0700

    splitting project profile page and global overview

[33mcommit 24f410c1d427962d7c32cf5eb22cea51db135ad7[m
Author: Shuwei Yang <shuweiy@uci.edu>
Date:   Mon Apr 27 21:13:32 2015 -0700

    The css conflict solved

[33mcommit 07704bdd802659c199823048056a7327f3826d92[m
Author: Daniel <endqwerty@gmail.com>
Date:   Tue Apr 28 01:41:02 2015 +0000

    passing data using array

[33mcommit e90b3f088f406a163d56ed4198cb8cce615436fa[m
Author: Daniel <endqwerty@gmail.com>
Date:   Tue Apr 28 01:24:49 2015 +0000

    pass data to view

[33mcommit a7935bb0d564936a5d4cb2545781bef427571f5b[m
Author: Daniel <endqwerty@gmail.com>
Date:   Tue Apr 28 01:13:43 2015 +0000

    routes simplification

[33mcommit f56af7a5e009e294a7113a28bab186fe35c5c7be[m
Author: Shuwei Yang <shuweiy@uci.edu>
Date:   Thu Apr 23 19:12:20 2015 -0700

    Add timeline into the project page, But css overlap

[33mcommit 1a6a2ca9725a6154c1915c4469d85bed13b0fde6[m
Author: Shuwei Yang <shuweiy@uci.edu>
Date:   Thu Apr 23 11:34:02 2015 -0700

    Fixed layout

[33mcommit 76479fcec20417b9ec3a38e52c10aedf327eceed[m
Author: Shuwei Yang <shuweiy@uci.edu>
Date:   Wed Apr 22 23:31:42 2015 -0700

    The project page view

[33mcommit 97b7e9ac7b88d56566d81204e32555e3137b485e[m
Author: Daniel <endqwerty@gmail.com>
Date:   Wed Apr 22 16:30:26 2015 -0700

    routing adjustments
    adding in dynamic routing to projects

[33mcommit b1466403f76b9848cefeca06fd678721185f8a88[m
Author: Daniel <endqwerty@gmail.com>
Date:   Wed Apr 22 14:38:34 2015 -0700

    fix css in blade format

[33mcommit aa17f7218369cd05e28d9381cfaeae68d5017e10[m
Author: Daniel <endqwerty@gmail.com>
Date:   Wed Apr 22 01:32:28 2015 -0700

    gitignore fixes and file renames

[33mcommit 263a5a745311751aeee9f58240be59801d63538e[m
Author: Daniel <endqwerty@gmail.com>
Date:   Mon Apr 20 22:47:48 2015 +0000

    fix int to integer

[33mcommit 9c862689df444f3ce26f92652625c2d5ff0e4b59[m
Author: Daniel <endqwerty@gmail.com>
Date:   Mon Apr 20 22:26:38 2015 +0000

    add timeline js

[33mcommit 20e2fd8ceebbcafb6ee7ef7c30a6f51c573791f7[m
Author: Daniel <endqwerty@gmail.com>
Date:   Mon Apr 20 05:54:29 2015 +0000

    add task and project models

[33mcommit 319da48e9e6fcfab42a566fe9f0cb24b2acdc08b[m
Author: Daniel <endqwerty@gmail.com>
Date:   Mon Apr 20 05:45:32 2015 +0000

    modified gitignore

[33mcommit 7425995baab770d423a03cbe3715d98a410a688d[m
Author: Daniel <endqwerty@gmail.com>
Date:   Mon Apr 13 06:20:09 2015 +0000

    reduce mobile compatibility for consistancy

[33mcommit 3694a8ee49695e4428667eb7942431749fdba154[m
Author: Daniel <endqwerty@gmail.com>
Date:   Sun Apr 12 22:52:16 2015 +0000

    added routes

[33mcommit b93f19602f941b7d231ce33784f38b58e42bf22b[m
Author: Daniel <endqwerty@gmail.com>
Date:   Sun Apr 12 22:32:45 2015 +0000

    add new pages

[33mcommit 700ac60b42452d53cdf2fa97beaf7dfd84e95dac[m
Author: Daniel <endqwerty@gmail.com>
Date:   Sun Apr 12 22:32:20 2015 +0000

    setup new pages

[33mcommit 59555b07b3df772f83948430b11fc92bfef5cc4c[m
Author: Daniel <endqwerty@gmail.com>
Date:   Mon Apr 6 02:20:42 2015 +0000

    small-caps text, rounded corners on images.

[33mcommit e0a72f4ad2087a1ccb96551cc2cd3977fe71befe[m
Author: vagrant <vagrant@homestead>
Date:   Mon Apr 6 02:13:02 2015 +0000

    added source sans pro font and built groundwork for responsive layout. only supports >=992px right now"
    ;

[33mcommit f4687f283134d1f7e6800f8ccb7f0fe3a0c84111[m
Author: Daniel <endqwerty@gmail.com>
Date:   Sun Apr 5 18:32:56 2015 -0700

    fixed image hover functionality to use img tags

[33mcommit b6d0960f980855620c738f80ae56a0d2bf4c2aa1[m
Author: Daniel <endqwerty@gmail.com>
Date:   Fri Apr 3 16:09:08 2015 -0700

    add image

[33mcommit 490741082f5c3e8c6f3e6aab540e2ffe51bb3b42[m
Author: Daniel <endqwerty@gmail.com>
Date:   Fri Apr 3 16:07:01 2015 -0700

    change from using bootstrap for custom design

[33mcommit f91524096abb6d465b3a612326dd0cc6c9ab2dc4[m
Author: vagrant <vagrant@homestead>
Date:   Thu Apr 2 17:13:48 2015 +0000

    added hover text

[33mcommit 2be42724fc4f61b490b581297953c489bb32351a[m
Author: vagrant <vagrant@homestead>
Date:   Thu Apr 2 04:37:50 2015 +0000

    center ar block

[33mcommit 1e8e2d8e0c7134ad57de9bd0ca7c11d5124c75ce[m
Author: vagrant <vagrant@homestead>
Date:   Thu Apr 2 04:21:49 2015 +0000

    added paneling

[33mcommit bd4e6fecb8b338832ed95e608941a2ea062c76f4[m
Author: vagrant <vagrant@homestead>
Date:   Thu Apr 2 04:09:12 2015 +0000

    framed homepage with bootstrap columns

[33mcommit 96291d8b992e7a2b791a9a5fc37dc6c93d2ee0ab[m
Author: vagrant <vagrant@homestead>
Date:   Fri Mar 27 06:40:39 2015 +0000

    change welcome screen

[33mcommit 012aef14a3166fef481af437be32db9808016890[m
Author: vagrant <vagrant@homestead>
Date:   Fri Mar 27 05:24:22 2015 +0000

    change welcome page

[33mcommit 6cc10f6af6cf262bdeb5f9a695fff7d3941c9f76[m
Author: vagrant <vagrant@homestead>
Date:   Fri Mar 27 00:12:04 2015 +0000

    edited the secret key to make it match with laravel's structure.

[33mcommit 33cd9c045d2ff1614c71b96716dfddca421d5a74[m
Author: vagrant <vagrant@homestead>
Date:   Thu Mar 26 13:03:35 2015 +0000

    begin creating database

[33mcommit c5d4011a1eec40d3312164c5c8faaabe454403c3[m
Author: Daniel <endqwerty@gmail.com>
Date:   Thu Mar 26 01:59:56 2015 -0700

    first init
