<div id="menu" class="hidden-print hidden-xs sidebar-blue sidebar-brand-primary">
	<div id="sidebar-fusion-wrapper">
		<div id="brandWrapper">
			<a href="{!! URL::route('project')!!}"><span class="text">Company</span></a>
		</div>
		<div id="logoWrapper">
			<div id="logo">
				<a href = "" class = "btn btn-sm btn-inverse">
					<i class  = "fa fa-fw icon-home-fill-1"></i>
				</a>

				<a href = "" class = "btn btn-sm btn-inverse">
					<i class  = "fa fa-fw icon-home-fill-1"></i>
				</a>

				<div class = "innerTB">
					<select name id = "menu_switch" data-style="btn-inverse" class = "selectpicker
					margin-none dropdown-menu-light" data-container="body" style ="display:none;">
						<option value = "">I feel Happy</option>
						<option value = "">I feel Lucky</option>
						<option value = "">I feel Sad</option>
						<option value = "">I feel Excited</option>
					</select>

				</div>

				<div id="toggleNavbarColor" data-toggle="navbar-color">
					<a href="" class="not-animated color color-blue active"></a>
					<a href="" class="not-animated color color-white"></a>
					<a href="" class="not-animated color bg-primary"></a>
					<a href="" class="not-animated color color-inverse"></a>
				</div>

			</div>
		</div>
	    <ul class="menu list-unstyled" id="navigation_current_page">
	       	<li class="treeview {!! ($pageNo == 1) ? 'active' : '' !!}">
				<a href="{!! URL::route('project')!!}">
                	<i class="fa fa-dashboard"></i>
                    <span>Manage Project</span>
				</a>

			</li class="treeview {!! ($pageNo == 1) ? 'active' : '' !!}">

			<li class="treeview {!! ($pageNo == 1) ? 'active' : '' !!}">
				<a href="{!! URL::route('project')!!}">
					<i class="fa fa-user"></i>
					<span>Team Management</span>
				</a>

			</li class="treeview {!! ($pageNo == 1) ? 'active' : '' !!}">

			<li class="treeview {!! ($pageNo == 2) ? 'active' : '' !!}">
				<a href="{!! URL::route('project.create')!!}">
					<i class="fa fa-file-text-o"></i>
					<span>Adding Project</span>
				</a>

			</li class="treeview {!! ($pageNo == 2) ? 'active' : '' !!}">

			<li class="treeview {!! ($pageNo == 3) ? 'active' : '' !!}">
				<a href="{!! URL::route('project')!!}">
					<i class="fa fa-bookmark-o"></i>
					<span>Management Application</span>
				</a>

			</li class="treeview {!! ($pageNo == 3) ? 'active' : '' !!}">






		</ul>
	</div>
</div>

